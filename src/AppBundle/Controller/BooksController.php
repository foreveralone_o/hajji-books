<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Author;
use AppBundle\Entity\Book;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class BooksController extends FOSRestController
{
    /**
     * @Rest\Get("/books/")
     */
    public function booksAction(Request $request)
    {
        $result = $this->getDoctrine()->getRepository(Book::class)->findAll();
        if ($result == null) {
            return new View("There Are No Books", Response::HTTP_NOT_FOUND);
        }
        return $result;
    }

    /**
     * @Rest\Get("/books/{s}/{order}/")
     */
    public function searchAndOrderAction($s, $order, Request $request)
    {
        $result = $this->getDoctrine()->getRepository(Book::class)->findAll();
        if ($result == null) {
            return new View("There Are No Books", Response::HTTP_NOT_FOUND);
        }
        $em = $this->getDoctrine()->getManager();
        $result = $em->getRepository(Book::class)->createQueryBuilder('b')
            ->where('b.title LIKE :title')
            ->orderBy('b.title', $order)
            ->setParameter('title', '%' . $s . '%')
            ->getQuery()
            ->getResult();
        return $result;
    }

    /**
     * @Rest\Get("/books/{id}/")
     */
    public function getSingleAction($id, Request $request)
    {

        //$title = $request->get('title');
        $result = $this->getDoctrine()->getRepository(Book::class)->findOneById($id);

        if ($result == null) {
            return new View("There Are No Books", Response::HTTP_NOT_FOUND);
        }
        return $result;
    }

    /**
     * @Rest\Post("/books/")
     */
    public function addTask(Request $request)
    {

        $title = $request->get('title');
        $author = $request->get('author');
        if (empty($title) || empty($title)) {
            return new View("NULL VALUES ARE NOT ALLOWED", Response::HTTP_NOT_ACCEPTABLE);
        }
        $em = $this->getDoctrine()->getManager();
        $authorAdd = new Author();
        $authorAdd->setFullName($author);
        $authorAdd->setEmail($author . '@email.com');
        $em->persist($authorAdd);
        $book = new Book();
        $book->setTitle($title);
        $book->setDescription("temporary foobar description");
        $book->setIsbn(mt_rand());
        $book->setAddedOn(new \DateTime());
        $book->setAuthor($authorAdd);
        $em->persist($book);
        $em->flush();
        return new View("Book Added Successfully", Response::HTTP_ACCEPTED);

    }

    /**
     * @Rest\Put("/books/{id}/")
     */

    public function updateAction(Request $request, $id)
    {

        $em = $this->getDoctrine()->getManager();
        $book = $em->getRepository(Book::class)->find($id);
        $book->setTitle($request->get('title'));
        $book->setDescription($request->get('description'));
        $book->setIsbn($request->get('isbn'));
        $book->setAddedOn(new \DateTime());

        
        $em->persist($book);
        $em->flush();

        return new View("Book ".$id." Successfully Edited", Response::HTTP_ACCEPTED);

    }

    /**
     * @Rest\Patch("/books/{id}/")
     */

    public function patchAction(Request $request, $id)
    {

        $em = $this->getDoctrine()->getManager();
        $book = $em->getRepository(Book::class)->find($id);
        $book->setTitle($request->get('title'));
        
        $em->persist($book);
        $em->flush();

        return new View("Book ".$id." Successfully Edited", Response::HTTP_ACCEPTED);

    }

    /**
     * @Rest\Delete("/books/{id}/")
     */

    public function delAction(Request $request, $id)
    {

        $em = $this->getDoctrine()->getManager();
        $book = $em->getRepository(Book::class)->find($id);
        $em->remove($book);
        $em->flush();

        return new View("Book ".$id." Successfully Deleted", Response::HTTP_ACCEPTED);

    }

}
