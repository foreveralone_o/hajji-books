<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Book;
use AppBundle\Entity\Author;
use Doctrine\Bundle\FixturesBundle\Fixtures;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;

class LoadFixtures implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $names = [
            1 => "JK Rowling",
            2 => "Stephen King",
            3 => "George R.R Martin",
        ];

        $books = [
            1 => [
                "title" => "Harry Potter",
                "description" => "The story of the boy who lived",
                "isbn" => "97815932",
                "addedOn" => "2014-12-14",
                "author_id" => 1

            ],
            2 => [
                "title" => "11/22/63",
                "description" => "JFK Assassination - Timetravel",
                "isbn" => "97814919",
                "addedOn" => "2014-07-01",
                "author_id" => 2
            ],
            3 => [
                "title" => "A Game Of Thrones",
                "description" => "Medeival Fantasy",
                "isbn" => "97815932",
                "addedOn" => "2016-09-03",
                "author_id" => 3
            ],
        ];

        for($i=1; $i<=3; $i++){

            $author = new Author();

            $author->setFullName($names[$i]);
            $author->setEmail('test@email.com');

            $book = new Book();

            $book->setTitle($books[$i]["title"]);
            $book->setDescription($books[$i]["description"]);
            $book->setIsbn($books[$i]["isbn"]);
            $book->setAddedOn(new \DateTime());
            $book->setAuthor($author);

            $manager->persist($author);
            $manager->persist($book);

        }
        
        $manager->flush();

    }
}
